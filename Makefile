MYLIB=/Users/huyle/sep/lib
S=./src
T=./tests
B=./bin
O=./obj

cc=c++ -std=c++11 -I$(MYLIB) -I$(S) -ljpeg -lfftw3

.DEFAULT_GOAL := exe

.SECONDARY:

exe: $(B)/test_imageio.x $(B)/test_image.x $(B)/test_quantize.x $(B)/test_pbm.x $(B)/test_transform.x $(B)/test_math.x $(B)/test_fftw.x $(B)/test_huffman.x $(B)/test_histo.x $(B)/test_lloyd.x

$(O)/myio.o: $(MYLIB)/myio.cpp
	$(cc) -c $< -o $@

$(O)/%.o: $(S)/%.cpp
	$(cc) -c $< -o $@

$(O)/%.o: $(T)/%.cpp
	$(cc) -c $< -o $@

$(B)/test_imageio.x: $(O)/test_imageio.o $(O)/myio.o $(O)/imageio.o
	$(cc) $^ -o $@

$(B)/test_image.x: $(O)/test_image.o $(O)/myio.o $(O)/imageio.o $(O)/image.o
	$(cc) $^ -o $@

$(B)/test_quantize.x: $(O)/test_quantize.o $(O)/myio.o $(O)/imageio.o $(O)/image.o $(O)/improcessing.o $(O)/immath.o
	$(cc) $^ -o $@

$(B)/test_pbm.x: $(O)/test_pbm.o $(O)/myio.o $(O)/imageio.o $(O)/image.o $(O)/improcessing.o $(O)/immath.o
	$(cc) $^ -o $@

$(B)/test_math.x: $(O)/test_math.o $(O)/immath.o
	$(cc) $^ -o $@

$(B)/test_transform.x: $(O)/test_transform.o $(O)/myio.o $(O)/imageio.o $(O)/image.o $(O)/improcessing.o $(O)/immath.o
	$(cc) $^ -o $@

$(B)/test_fftw.x: $(O)/test_fftw.o $(O)/myio.o $(O)/imageio.o $(O)/image.o
	$(cc) $^ -o $@

$(B)/test_huffman.x: $(O)/test_huffman.o
	$(cc) $^ -o $@

$(B)/test_subimage.x: $(O)/test_subimage.o $(O)/myio.o $(O)/imageio.o $(O)/image.o
	$(cc) $^ -o $@

$(B)/%.x: $(O)/%.o $(O)/myio.o $(O)/improcessing.o $(O)/immath.o $(O)/image.o $(O)/imageio.o
	$(cc) $^ -o $@

clean:
	rm -f $(O)/*.o $(B)/*.x *.jpg *.jpeg *.pbm *.ppm *.pgm
