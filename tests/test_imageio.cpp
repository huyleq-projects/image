#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>

#include "myio.h"
#include "imageio.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }

    int nh,nw,nc;
    J_COLOR_SPACE cs;
    float *fimage;
    if(read_jpeg_to_fgray(&fimage,&nh,&nw,&nc,&cs,infilename)){
        printf("nh=%d nw=%d nc=%d cs=%d\n",nh,nw,nc,cs);
        string outfilename=get_s("out");
        if(!outfilename.empty()) write_fgray_to_jpeg(fimage,nh,nw,outfilename);
        else printf("please specify output image file by out=filename\n");
        delete []fimage;
    }
    
    myio_close();
    return 0;
}
