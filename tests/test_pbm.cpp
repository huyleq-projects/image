#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>

#include "myio.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        printf("nh=%d nw=%d nc=%d size=%d cs=%d\n",image.height,image.width,image.component,image.size,image.colorspace);
        string outfilename=get_s("out");
        Image image1(image.height,image.width,1,JCS_GRAYSCALE);
        rgb2gray(&image1,&image);
        printf("nh=%d nw=%d nc=%d size=%d cs=%d\n",image1.height,image1.width,image1.component,image1.size,image1.colorspace);
        if(!outfilename.empty()){
            printf("outfilename=%s\n",outfilename.c_str());
            write_netpbm_image(&image1,outfilename);
        }
        else printf("please specify output image file by out=filename\n");
    }
    
    myio_close();
    return 0;
}
