#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>

#include "myio.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        printf("nh=%d nw=%d nc=%d size=%d cs=%d\n",image.height,image.width,image.component,image.size,image.colorspace);
        uint8_t ratio=1;
        get_param("ratio",ratio);
        quantize(&image,ratio);
        string outfilename=get_s("out");
        if(!outfilename.empty()) write_netpbm(image.data,image.height,image.width,image.component,outfilename.c_str());
        else printf("please specify output image file by out=filename\n");
    }
    
    myio_close();
    return 0;
}
