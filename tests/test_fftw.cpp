#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>

#include <fftw3.h>

#include "myio.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        int wh=image.height*image.width;
        double *x=new double[wh];
        image2gray(x,&image);
        double *fx=new double[wh];
        double *z=new double[wh];
        fftw_plan plan=fftw_plan_r2r_2d(image.height,image.width,x,fx,FFTW_REDFT10,FFTW_REDFT10,FFTW_ESTIMATE);
        fftw_execute(plan);
        fftw_destroy_plan(plan);
        fftw_plan plan1=fftw_plan_r2r_2d(image.height,image.width,fx,z,FFTW_REDFT01,FFTW_REDFT01,FFTW_ESTIMATE);
        fftw_execute(plan1);
        fftw_destroy_plan(plan1);
        float *y=new float[wh];
        for(int i=0; i<wh; i++) y[i]=z[i]/wh;
        write("fx",y,wh);
        to_header("fx","n1",image.width,"o1",0,"d1",1);
        to_header("fx","n2",image.height,"o2",0,"d2",1);
        delete []x; delete []fx; delete []y;
    }
    
    myio_close();
    return 0;
}
