#include <chrono>
#include <random>

#include "immath.h"

using namespace std;

int main(int argc,char **argv){
    unsigned seed=chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);
    uniform_real_distribution<float> distribution(1.f,10.f);
    float a[6],a1[6],t[6];
    for(int i=0; i<6; i++) a[i]=distribution(generator);
    inverse_transform(a1,a);
    compose_transforms(t,a,a1);
    print(a); printf("\n");
    print(a1); printf("\n");
    print(t); printf("\n");
    return 0;
}
