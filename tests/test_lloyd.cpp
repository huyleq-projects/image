#include <cmath>
#include <cstring>
#include <random>
#include "myio.h"
#include "image.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);

    float h[256];
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        printf("nh=%d nw=%d nc=%d cs=%d\n",image.height,image.width,image.component,image.colorspace);

        histogram(h,image.data,image.size);
        
        to_header("h0","n1",256,"o1",0,"d1",1);
        write("h0",h,256);

        int nlevel; get_param("nlevel",nlevel);
        float *a=new float[nlevel];
        float *b=new float[nlevel+1];
        vector<float> mse;
        lloyd_max_quantizer(mse,a,b,nlevel,image.data,image.size);

        to_header("mse","n1",mse.size(),"o1",0,"d1",1);
        write("mse",mse.data(),mse.size());
        
        for(int i=0; i<nlevel; i++){
            int left=ceil(b[i]), right=ceil(b[i+1]);
            printf("from %d to %d: %f\n",left,right,a[i]);
            float sum=0;
            for(int j=left; j<right; j++) sum+=h[j];
            sum/=(right=left);
            for(int j=left; j<right; j++) h[j]=sum;
        }
        to_header("h1","n1",256,"o1",0,"d1",1);
        write("h1",h,256);

        delete []a; delete []b;
    }

    myio_close();
    return 0;
}
