#include <cmath>
#include <cstring>
#include <random>
#include "myio.h"
#include "image.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);

    float h[256];
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        printf("nh=%d nw=%d nc=%d cs=%d\n",image.height,image.width,image.component,image.colorspace);

        histogram(h,image.data,image.size);
        
        to_header("h0","n1",256,"o1",0,"d1",1);
        write("h0",h,256);
        
        int nlevel; get_param("nlevel",nlevel);
        lloyd_max_quantize(&image,nlevel);
        
        histogram(h,image.data,image.size);
        
        to_header("h1","n1",256,"o1",0,"d1",1);
        write("h1",h,256);
        
        string outfilename=get_s("out");
        if(!outfilename.empty()) write_jpeg_image(&image,outfilename);
        else printf("please specify output image file by out=filename\n");
    }

    myio_close();
    return 0;
}
