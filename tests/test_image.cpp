#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>

#include "myio.h"
#include "imageio.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        printf("nh=%d nw=%d nc=%d cs=%d\n",image.height,image.width,image.component,image.colorspace);
        string outfilename=get_s("out");
        Image image1=image;
        if(!outfilename.empty()) write_jpeg_image(&image1,outfilename);
        else printf("please specify output image file by out=filename\n");
    }
    
    myio_close();
    return 0;
}
