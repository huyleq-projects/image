#include <cmath>
#include <random>
#include "myio.h"
#include "image.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);

    int n=1<<20;
    uint8_t *x=new uint8_t[n];

    default_random_engine gen;
    uniform_int_distribution<uint8_t> udist(0,255);
    normal_distribution<float> ndist(100,20);
    
    float h[256];
    float cdf[256];
    
    for(int i=0; i<n; i++) x[i]=udist(gen);
    
    histogram(h,x,n);
    
    to_header("h0","n1",256,"o1",0,"d1",1);
    write("h0",h,256);

    histo2cdf(cdf,h,256);
    
    to_header("cdf0","n1",256,"o1",0,"d1",1);
    write("cdf0",cdf,256);

    for(int i=0; i<n; i++){
        float r=ndist(gen);
        x[i]=min(max(r,0.f),255.f);
    }
    
    histogram(h,x,n);
    
    to_header("h1","n1",256,"o1",0,"d1",1);
    write("h1",h,256);

    histo2cdf(cdf,h,256);
    
    to_header("cdf1","n1",256,"o1",0,"d1",1);
    write("cdf1",cdf,256);

    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        printf("nh=%d nw=%d nc=%d cs=%d\n",image.height,image.width,image.component,image.colorspace);

        histogram(h,image.data,image.size);
        
        to_header("h2","n1",256,"o1",0,"d1",1);
        write("h2",h,256);
    
        histo2cdf(cdf,h,256);
        
        to_header("cdf2","n1",256,"o1",0,"d1",1);
        write("cdf2",cdf,256);

        histogram_eq(image.data,image.data,image.size);
        
        histogram(h,image.data,image.size);
        
        to_header("h3","n1",256,"o1",0,"d1",1);
        write("h3",h,256);
    
        histo2cdf(cdf,h,256);
        
        to_header("cdf3","n1",256,"o1",0,"d1",1);
        write("cdf3",cdf,256);
        
        string outfilename=get_s("out");
        if(!outfilename.empty()) write_jpeg_image(&image,outfilename);
        else printf("please specify output image file by out=filename\n");

    }

    delete []x;
    myio_close();
    return 0;
}
