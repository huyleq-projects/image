#include <cmath>
#include <random>
#include "myio.h"
#include "image.h"
#include "imageio.h"
#include "improcessing.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);

    string infilename1=get_s("image1");
    if(infilename1.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    string infilename2=get_s("image2");
    if(infilename2.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
   
    float h[256];

    Image image1;
    read_jpeg_image(&image1,infilename1);
    histogram(h,image1.data,image1.size); 
    to_header("h1","n1",256,"o1",0,"d1",1);
    write("h1",h,256);

    Image image2;
    read_jpeg_image(&image2,infilename2);
    assert(image2.same_space(image1));
    histogram(h,image2.data,image2.size); 
    to_header("h2","n1",256,"o1",0,"d1",1);
    write("h2",h,256);

    histogram_march(image2.data,image1.data,image1.size);
    histogram(h,image2.data,image2.size); 
    to_header("hout","n1",256,"o1",0,"d1",1);
    write("hout",h,256);

    string outfilename=get_s("out");
    if(!outfilename.empty()) write_jpeg_image(&image2,outfilename);
    else printf("please specify output image file by out=filename\n");
    
    myio_close();
    return 0;
}
