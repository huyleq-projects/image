#include <iostream>
#include <algorithm>

#include "huffman.h"

using namespace std;

int main(int argc,char **argv){
    vector<char> v;
    v.push_back('h');
    v.push_back('q');
    v.push_back('d');
    v.push_back('d');
    v.push_back('o');
    v.push_back('o');
    v.push_back('o');
    v.push_back('z');
    v.push_back('z');
    v.push_back('z');
    random_shuffle(v.begin(),v.end());
    
    string code;
    HuffmanNode<char> *node=huffman_encode(code,v.data(),v.size());
    
    for(const char &c:v) cout<<c<<endl;
    cout<<code<<endl;
    
    vector<char> v1(v.size());
    huffman_decode(v1.data(),v1.size(),code,node);
    for(const char &c:v1) cout<<c<<endl;

    huffman_free(node);
    return 0;
}
