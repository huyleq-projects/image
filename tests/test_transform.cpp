#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>

#include "myio.h"
#include "imageio.h"
#include "improcessing.h"
#include "immath.h"

using namespace std;

int main(int argc,char **argv){
    myio_init(argc,argv);
    
    string infilename=get_s("image");
    if(infilename.empty()){
        printf("please specify input image by image=filename\n");
        return 0;
    }
    
    Image image;
    if(read_jpeg_image(&image,infilename)){
        Image image1; 
        image1.allocate(image);
        
        float t[6];
        get_rotate_matrix(t,60,image.width/2,image.height/2);
        transform(&image1,&image,t);
        
        string outfilename=get_s("out");
        if(!outfilename.empty()){
            printf("outfilename=%s\n",outfilename.c_str());
            write_netpbm_image(&image1,outfilename);
        }
        else printf("please specify output image file by out=filename\n");
    }
    
    myio_close();
    return 0;
}
