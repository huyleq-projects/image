#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "immath.h"

void compose_transforms(float t[6],const float a[6],const float b[6]){
    assert(t!=a && t!=b);
    t[0]=b[0]*a[0]+b[1]*a[3];
    t[1]=b[0]*a[1]+b[1]*a[4];
    t[2]=b[0]*a[2]+b[1]*a[5]+b[2];
    t[3]=b[3]*a[0]+b[4]*a[3];
    t[4]=b[3]*a[1]+b[4]*a[4];
    t[5]=b[3]*a[2]+b[4]*a[5]+b[5];
    return;
}

void print(const float t[6]){
    for(int i=0; i<2; i++){
        for(int j=0; j<3; j++){
            int k=i*3+j;
            printf("%e     ",t[k]);
        }
        printf("\n");
    }
    printf("%e     %e     %e",0.f,0.f,1.f);
    return;
}

void inverse_transform(float t[6],const float a[6]){
    assert(t!=a);
    float det=determinant(a);
    assert(det!=0);
    t[0]=a[4]/det;
    t[1]=-a[1]/det;
    t[2]=(a[1]*a[5]-a[2]*a[4])/det;
    t[3]=-a[3]/det;
    t[4]=a[0]/det;
    t[5]=(a[2]*a[3]-a[0]*a[5])/det;
    return;
}

void get_shearX_matrix(float t[6],float a){
    t[0]=t[4]=1;
    t[1]=a; t[2]=t[3]=t[5]=0;
    return;
}

void get_shearY_matrix(float t[6],float a){
    t[0]=t[4]=1;
    t[3]=a; t[1]=t[2]=t[5]=0;
    return;
}

void get_translate_matrix(float t[6],float x,float y){
    t[2]=x; t[5]=y;
    t[0]=t[1]=t[3]=t[4]=0;
    return;
}

void get_rotate_matrix(float t[6],float theta,float x,float y){
    theta=theta/180.f*PI;
    float c=cos(theta), s=sin(theta);
    t[0]=c; t[1]=-s; t[3]=s; t[4]=c;
    c=1-c;
    t[2]=x*c+y*s;
    t[5]=y*c-x*s;
    return;
}

void get_scale_matrix(float t[6],float sx,float sy,float x,float y){
    t[0]=sx; t[4]=sy; 
    t[1]=t[3]=0;
    t[2]=x*(1.f-sx);
    t[5]=y*(1.f-sy);
    return;
}

