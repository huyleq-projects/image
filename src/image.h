#ifndef IMAGE_H
#define IMAGE_H

#include <cstdio>
#include <cstdlib>
#include <jpeglib.h>

struct Image{
    int height,width,component;
    int size;
    J_COLOR_SPACE colorspace;
    uint8_t *data;
    
    Image():height(0),width(0),component(0),size(0),colorspace(JCS_UNKNOWN),data(nullptr){}
    Image(int h,int w,int c,J_COLOR_SPACE cs);
    Image(const Image &other);
    Image& operator=(const Image &other);
    ~Image(){ if(data) delete []data;}
    void allocate(int h,int w,int c,J_COLOR_SPACE cs);
    void allocate(const Image &other);
    bool empty() const;
    bool same_size(const Image &other) const;
    bool same_colorspace(const Image &other) const;
    bool same_space(const Image &other) const;
    bool is_gray() const;
    bool is_rgb() const;
    bool is_ycbcr() const;
};

#endif
