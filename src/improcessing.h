#ifndef IMPROCESSING_H
#define IMPROCESSING_H

#include <cmath>

#include "image.h"

#define TOL 0.001

inline
void quantize(uint8_t *val,uint8_t ratio){
    *val=((*val)/ratio)*ratio;
    return;
}

void quantize(Image *image,uint8_t ratio);

template<typename T>
inline 
void rgb2gray(T *gray,uint8_t red,uint8_t green,uint8_t blue){
    *gray=0.2126*red+0.7152*green+0.0722*blue;
    return;
}

template<typename T>
inline 
void rgb2gray(T *gray,const uint8_t *pixel){
    rgb2gray(gray,pixel[0],pixel[1],pixel[2]);
    return;
}

template<typename T>
void image2gray(T *x,const Image *image){
    if(image->component==1){
        for(int i=0; i<image->size; i++) x[i]=image->data[i];
    }
    else{
        int wh=image->height*image->width;
        for(int i=0; i<wh; i++) rgb2gray(x+i,image->data+i*3);
    }
    return;
}

void rgb2gray(Image *out,const Image *in);

void transform(Image *out,const Image *in,const float t[6]);

void histogram(float h[256],const uint8_t *v,int n);

void histo2cdf(float *cdf,const float *h,int n);

void histogram_eq(uint8_t *out,const uint8_t *in,int n);

template<typename T>
int binary_search(const T *v,int left,int right,T x){
    if(right-left==1) return left;
    else if(right-left==2){
        if(std::abs(x-v[left])<=std::abs(x-v[left+1])) return left;
        else return left+1;
    }
    else{
        int mid=(left+right)/2;
        if(v[mid]==x) return mid;
        else if(v[mid]>x) return binary_search(v,left,mid,x);
        else return binary_search(v,mid,right,x);
    }
}

template<typename T>
int binary_search(const T *v,int n,T x){
    return binary_search(v,0,n,x);
}

void histogram_march(uint8_t *inout,const uint8_t *in,int n);

float mean(const float h[256],int left=0,int right=256);

float compute_mse(const float h[256],const float *a,const float *b,int m);

void lloyd_max_quantizer(std::vector<float> &mse,float *a,float *b,int m,const uint8_t *x,int n);

void lloyd_max_quantize(Image *image,int nlevel);

#endif
