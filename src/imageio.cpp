#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <cassert>

#include <jpeglib.h>
#include "imageio.h"
#include "improcessing.h"

using namespace std;

bool read_jpeg_image(Image *image,const string &filename){
    bool stat=read_jpeg(&image->data,&image->height,&image->width,&image->component,&image->colorspace,filename);
    if(stat) image->size=image->height*image->width*image->component;
    return stat;
}

bool write_jpeg_image(const Image *image,const string &filename,int quality){
    return write_jpeg(image->data,image->height,image->width,image->component,image->colorspace,filename,quality);
}

bool read_jpeg_to_fgray(float **fimage,int *nh,int *nw,int *nc,J_COLOR_SPACE *cs,const string &filename){
    uint8_t *image;
    bool stat=read_jpeg(&image,nh,nw,nc,cs,filename);
    if(stat){
        *fimage=new float[(*nh)*(*nw)];
        if((*nc)==1){
            for(int i=0;i<(*nh)*(*nw);i++) (*fimage)[i]=image[i];
        }
        else{
            for(int i=0;i<(*nh)*(*nw);i++) rgb2gray((*fimage)+i,image+i*3);
        }
        delete []image;
        return stat;
    }
    else return false;
}

bool write_fgray_to_jpeg(float *fimage,int nh,int nw,const string &filename,int quality){
    uint8_t *image=new uint8_t[nh*nw];
    for(int i=0;i<nh*nw;i++) image[i]=fimage[i];
    bool stat=write_jpeg(image,nh,nw,1,JCS_GRAYSCALE,filename,quality);
    delete []image;
    return stat;
}

bool read_jpeg(uint8_t **image,int *nh,int *nw,int *nc,J_COLOR_SPACE *cs,const string &filename){
	FILE *infile=fopen(filename.c_str(),"rb");
	if(!infile){
		printf("Error opening jpeg file %s\n!",filename.c_str());
		return false;
	}

	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;
	cinfo.err=jpeg_std_error(&jerr);
	jpeg_create_decompress(&cinfo);
	jpeg_stdio_src(&cinfo,infile);
	jpeg_read_header(&cinfo,TRUE);
	jpeg_start_decompress( &cinfo );
    
    *nh=cinfo.output_height;
    *nw=cinfo.output_width;
    *nc=cinfo.num_components;
    *cs=cinfo.out_color_space;
    
    *image=new uint8_t[(*nh)*(*nw)*(*nc)];
	JSAMPROW row_pointer=new uint8_t[(*nw)*(*nc)];
	while(cinfo.output_scanline<(*nh)){
        uint8_t *ptr=*image+cinfo.output_scanline*(*nw)*(*nc);
		jpeg_read_scanlines(&cinfo,&row_pointer,1);
        memcpy(ptr,row_pointer,(*nw)*(*nc)*sizeof(uint8_t));
	}

	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
    delete []row_pointer;
	fclose(infile );

	return true;
}

bool write_jpeg(uint8_t *image,int nh,int nw,int nc,J_COLOR_SPACE cs,const string &filename,int quality){
	FILE *outfile=fopen(filename.c_str(),"wb");
	if(!outfile){
		printf("Error opening output jpeg file %s\n!",filename.c_str());
		return false;
	
    }

	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
	cinfo.err=jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);
	jpeg_stdio_dest(&cinfo,outfile);

	cinfo.image_width=nw;	
	cinfo.image_height=nh;
	cinfo.input_components=nc;
	cinfo.in_color_space=cs;
	jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo,quality,TRUE);
	jpeg_start_compress(&cinfo,TRUE);
    
    int stride=nw*nc;
	while(cinfo.next_scanline<nh){
		JSAMPROW row_pointer=image+cinfo.next_scanline*stride;
		jpeg_write_scanlines(&cinfo,&row_pointer,1);
	}

	jpeg_finish_compress(&cinfo);
	jpeg_destroy_compress(&cinfo);
	fclose(outfile);

	return true;
}

bool write_netpbm_image(const Image *image,const string &filename){
    return write_netpbm(image->data,image->height,image->width,image->component,filename.c_str());
}

bool write_netpbm(const uint8_t *data,int height,int width,int component,const char *filename){
    FILE *fout=fopen(filename,"wb");
    if(fout){
        assert(component==1 || component==3);
        if(component==1) fprintf(fout,"P5\n%d %d\n255\n",width,height);
        else fprintf(fout,"P6\n%d %d\n255\n",width,height);
        fwrite(data,1,width*height*component,fout);
        fclose(fout);
        return true;
    }
    else{
        printf("cannot open file %s\n",filename);
        return false;
    }
}

