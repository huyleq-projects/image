#include <cassert>
#include <cstring>
#include <vector>
#include <cmath>

#include "improcessing.h"
#include "immath.h"


using namespace std;

void quantize(Image *image,uint8_t ratio){
    for(int i=0; i<image->size; i++){
        quantize(image->data+i,ratio);
    }
    return;
}

void rgb2gray(Image *out,const Image *in){
    assert(in->same_size(*out) && in->is_rgb() && out->is_gray());
    for(int i=0; i<out->size; i++) rgb2gray(out->data+i,in->data+i*3);
    return;
}

void transform(Image *out,const Image *in,const float t[6]){
    assert(out->component==in->component);
    float t1[6]; 
    inverse_transform(t1,t);
    for(int i=0; i<out->height; i++){
        for(int j=0; j<out->width; j++){
            int ij=(i*out->width+j)*out->component;
            float x,y; 
            coord_transform(&x,&y,j,i,t);
            if(x>=-0.5 && x<in->width && y>=0 && y<in->height){
                int r=round(y), c=round(x), rc=(r*in->width+c)*in->component;
                memcpy(out->data+ij,in->data+rc,in->component*sizeof(uint8_t));
            }
            else memset(out->data+ij,0,in->component*sizeof(uint8_t));
        }
    }
    return;
}

void histogram(float h[256],const uint8_t *v,int n){
    assert(n!=0);
    memset(h,0,256*sizeof(float));
    for(int i=0; i<n; i++) h[v[i]]++;
    for(int i=0; i<256; i++) h[i]/=n;
    return;
}

void histo2cdf(float *cdf,const float *h,int n){
    assert(n!=0);
    double s=0;
    for(int i=0; i<n; i++) cdf[i]=(s+=h[i]);
    return;
}

void histogram_eq(uint8_t *out,const uint8_t *in,int n){
    assert(n!=0);
    float h[256];
    histogram(h,in,n);
    histo2cdf(h,h,256);
    for(int i=0; i<n; i++) out[i]=255*h[in[i]];
    return;
}

void histogram_march(uint8_t *inout,const uint8_t *in,int n){
    assert(n!=0);
    float h_inout[256], h_in[256];
    histogram(h_inout,inout,n);
    histo2cdf(h_inout,h_inout,256);
    histogram(h_in,in,n);
    histo2cdf(h_in,h_in,256);
    for(int i=0; i<n; i++){
        int k=binary_search(h_in,256,h_inout[inout[i]]);
        inout[i]=min(max(k,0),255);
    }
    return;
}

float mean(const float h[256],int left,int right){
    float top=0, bot=0;
    for(int i=left; i<right; i++){
        top+=i*h[i]; bot+=h[i];
    }
    return top/bot;
}

float compute_mse(const float h[256],const float *a,const float *b,int m){
    float val=0;
    for(int i=0; i<m; i++){
        int left=ceil(b[i]), right=ceil(b[i+1]);
        float top=0, bot=0;
        for(int j=left; j<right; j++){
            float e=j-a[i]; top+=h[j]*e*e; bot+=h[j];
        }
        val+=top/bot;
    }
    return val;
}

void lloyd_max_quantizer(vector<float> &mse,float *a,float *b,int m,const uint8_t *x,int n){
    float h[256];
    histogram(h,x,n);

    if(m==1){
        a[0]=mean(h); b[0]=0; b[1]=255.5;
        return;
    }
    
    float d=255.f/(m-1);
    a[0]=0;
    for(int i=1; i<m; i++) a[i]=a[i-1]+d;
    
    b[0]=0; b[m]=255.5;

    while(true){
        for(int i=1; i<m; i++) b[i]=(a[i]+a[i-1])/2;
        mse.push_back(compute_mse(h,a,b,m));
        int s=mse.size();
        if(s>1 && (mse[s-2]-mse[s-1])<TOL*mse[s-2]) break;
        for(int i=0; i<m; i++) a[i]=mean(h,ceil(b[i]),ceil(b[i+1]));
    }
    return;
}

void lloyd_max_quantize(Image *image,int nlevel){

    float *a=new float[nlevel];
    float *b=new float[nlevel+1];
    vector<float> mse;
    uint8_t *ptr=image->data;
    int n=image->size;
    lloyd_max_quantizer(mse,a,b,nlevel,ptr,n);
    
    uint8_t func[256];
    for(int i=0; i<nlevel; i++){
        int left=ceil(b[i]), right=ceil(b[i+1]);
        for(int j=left; j<right; j++) func[j]=round(a[i]);
    }
    
    for(int i=0; i<n; i++) ptr[i]=func[ptr[i]];
    return;
}
