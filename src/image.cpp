#include <cstring>

#include "image.h"

Image::Image(int h,int w,int c,J_COLOR_SPACE cs){
    allocate(h,w,c,cs);
    return;
}

Image::Image(const Image &other){
    allocate(other.height,other.width,other.component,other.colorspace);
    memcpy(data,other.data,size*sizeof(uint8_t));
    return;
}

Image& Image::operator=(const Image &other){
    if(&other!=this){
        if(height!=other.height || width!=other.width || component!=other.component || colorspace!=other.colorspace){
            if(data) delete []data;
            allocate(other);
        }
        memcpy(data,other.data,size*sizeof(uint8_t));
    }
    return *this; 
}

void Image::allocate(int h,int w,int c,J_COLOR_SPACE cs){
    height=h; width=w; component=c; size=h*w*c; colorspace=cs; 
    data=new uint8_t[size];
    return;
}

void Image::allocate(const Image &other){
    allocate(other.height,other.width,other.component,other.colorspace);
    return;
}

bool Image::empty() const{
    if(data) return false;
    else return true;
}

bool Image::same_size(const Image &other) const{
    return (height==other.height && width==other.width);
}

bool Image::same_colorspace(const Image &other) const{
    return(component==other.component && colorspace==other.colorspace);
}

bool Image::same_space(const Image &other) const{
    return (same_size(other) && same_colorspace(other));
}

bool Image::is_gray() const{
    return(component==1 && colorspace==JCS_GRAYSCALE);
}

bool Image::is_rgb() const{
    return(component==3 && colorspace==JCS_RGB);
}

bool Image::is_ycbcr() const{
    return(component==3 && colorspace==JCS_YCbCr);
}
