#ifndef IMAGEIO_H
#define IMAGEIO_H

#include <string>
#include <jpeglib.h>
#include "image.h"

bool read_jpeg_image(Image *image,const std::string &filename);

bool write_jpeg_image(const Image *image,const std::string &filename,int quality=90);

bool read_jpeg_to_fgray(float **fimage,int *nh,int *nw,int *nc,J_COLOR_SPACE *cs,const std::string &filename);

bool write_fgray_to_jpeg(float *fimage,int nh,int nw,const std::string &filename,int quality=90);

bool read_jpeg(uint8_t **image,int *nh,int *nw,int *nc,J_COLOR_SPACE *cs,const std::string &filename);

bool write_jpeg(uint8_t *image,int nh,int nw,int nc,J_COLOR_SPACE cs,const std::string &filename,int quality=90);

bool write_netpbm_image(const Image *image,const std::string &filename);

bool write_netpbm(const uint8_t *data,int height,int width,int component,const char *filename);

#endif
