#ifndef IMMATH_H
#define IMMATH_H

#define PI 3.14159265359f

void compose_transforms(float t[6],const float a[6],const float b[6]);

inline
float determinant(const float t[6]){
    return t[0]*t[4]-t[3]*t[1];
}

void print(const float t[6]);

inline
void coord_transform(float *x,float *y,float x0,float y0,const float t[6]){
    *x=(t[0]*x0+t[1]*y0+t[2]);
    *y=(t[3]*x0+t[4]*y0+t[5]);
    return;
}

void inverse_transform(float t[6],const float a[6]);

void get_shearX_matrix(float t[6],float a);

void get_shearY_matrix(float t[6],float a);

void get_translate_matrix(float t[6],float x,float y);

void get_rotate_matrix(float t[6],float theta,float x=0,float y=0);

void get_scale_matrix(float t[6],float sx,float sy,float x=0,float y=0);

#endif
