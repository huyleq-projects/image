#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <iostream>
#include <vector>
#include <queue>
#include <unordered_map>

template<typename T>
struct HuffmanNode{
    T value;
    int frequency;
    HuffmanNode *zero; // left
    HuffmanNode *one; // right

    HuffmanNode():frequency(-1),zero(nullptr),one(nullptr){}
    HuffmanNode(T v,int f):value(v),frequency(f),zero(nullptr),one(nullptr){}
    HuffmanNode(int f,HuffmanNode *n1,HuffmanNode *n2):frequency(f),zero(n1),one(n2){}
};

template<typename T>
void huffman_free(HuffmanNode<T> *node){
    if(node){
        if(node->zero) huffman_free(node->zero);
        if(node->one) huffman_free(node->one);
        huffman_free(node);
    }
    return;
}

template<typename T>
bool huffman_compare(const HuffmanNode<T> *node1,const HuffmanNode<T> *node2){
    if(node1->frequency<node2->frequency) return true;
    else return false;
}

template<typename T>
void huffman_map_helper(std::unordered_map<T,std::string> &m,const HuffmanNode<T> *node,std::string s){
    if(!node->zero && !node->one) m[node->value]=s;
    else{
        huffman_map_helper(m,node->zero,s+"0");
        huffman_map_helper(m,node->one,s+"1");
    }
    return;
}

template<typename T>
void huffman_map(std::unordered_map<T,std::string> &m,const HuffmanNode<T> *node){
    std::string s;
    huffman_map_helper(m,node,s);
    return;
}

template<typename T>
HuffmanNode<T>* huffman_encode(std::string &code,const T *v,int n){
    if(!v || n<1) return nullptr;

    std::unordered_map<T,int> map;
    for(int i=0; i<n; i++){
        if(map.find(v[i])==map.end()) map[v[i]]=1;
        else map[v[i]]++;
    }
    
    std::priority_queue<HuffmanNode<T>*,std::vector<HuffmanNode<T>*>,decltype(&huffman_compare<T>)> pq(huffman_compare<T>);
    for(const auto &x:map){
        HuffmanNode<T> *node=new HuffmanNode<T>(x.first,-x.second);
        pq.push(node);
    }

    while(pq.size()>1){
        HuffmanNode<T> *node1=pq.top(); pq.pop();
        HuffmanNode<T> *node2=pq.top(); pq.pop();
        int f=node1->frequency+node2->frequency;
        HuffmanNode<T> *node=new HuffmanNode<T>(f,node1,node2);
        pq.push(node);
    }
    
    std::unordered_map<T,std::string> m;
    HuffmanNode<T> *node=pq.top();
    huffman_map(m,node);
    
    for(const auto &x:m) std::cout<<"code for "<<x.first<<" is "<<x.second<<std::endl;

    if(!code.empty()) code.clear();
    
    for(int i=0; i<n; i++) code+=m[v[i]];

    return node;
}

template<typename T>
void huffman_decode(T *v,int n,const std::string &code,const HuffmanNode<T> *node){
    int m=code.size(), k=0, i=0;
    while(k<m){
        const HuffmanNode<T> *head=node;
        while(head->zero && head->one){
            if(code[k]=='0') head=head->zero;
            else head=head->one;
            k++;
        }
        v[i++]=head->value;
    }
    return;
}


#endif
